package ru.rsreu.oop_prak_1;

import java.util.Scanner;

public class Array {

    private int[][] array;
    private int from, to, n, m;

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

    public int getM() {
        return m;
    }

    public void setM(int m) {
        this.m = m;
    }

    public Array(int n, int m) {
        array = new int[n][m];
        this.n = n;
        this.m = m;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public void setTo(int to) {
        this.to = to;
    }

    public boolean rangeCheck() {
        if (this.from >= this.to) {
            return true;
        } else {
            return false;
        }
    }

    private int elementGeneration() {
        return this.from + (int) (Math.random() * ((this.to - this.from) + 1));
    }

    public void autoInput() {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                this.array[i][j] = elementGeneration();
            }
        }
    }

    public void manualInput(int i, int j, int value) {
        this.array[i][j] = value;
    }


    public void getArray() {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                System.out.print(this.array[i][j] + ", ");
            }
            System.out.println();
        }
    }

    public String print() {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i <n ; i++) {
            for (int j = 0; j < m; j++) {
                stringBuffer.append(array[i][j] + " ");
            }
            stringBuffer.append("\n");
        }
        return stringBuffer.toString();
    }

    private boolean symmetry(int n) {
        String stemp = String.valueOf(n);
        String revstemp = new StringBuffer(stemp).reverse().toString();
        int rezult = Integer.parseInt(revstemp);
        return (n==rezult) ? true : false;
    }

    private int symmetryСheckMas(int[] mas) {
        int count = 0;
        for (int i = 0; i < m; i++) {
           if (symmetry(mas[i])) {
               count++;
           }
        }
        return count;
    }

    public int symmertyMax() {
        int max=symmetryСheckMas(array[0]);
        int number = 0;
        for (int i = 1; i < n; i++) {
            int temp = symmetryСheckMas(array[i]);
            if (temp>max) {
                max = temp;
                number = i;
            }
        }
        return number;
    }

    public int symmertyMin() {
        int min=symmetryСheckMas(array[0]);
        int number = 0;
        for (int i = 1; i < n; i++) {
            int temp = symmetryСheckMas(array[i]);
            if (temp<min) {
                min = temp;
                number = i;
            }
        }
        return number;
    }

}